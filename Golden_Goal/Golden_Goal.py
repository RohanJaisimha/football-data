import matplotlib.pyplot as plt
import textwrap


class Team:
    def __init__(self, name=""):
        self.name = name

        self.actual_goals_scored = 0
        self.actual_goals_conceded = 0
        self.actual_wins = 0
        self.actual_losses = 0
        self.actual_draws = 0
        self.actual_points = 0

        self.alternate_goals_scored = 0
        self.alternate_goals_conceded = 0
        self.alternate_wins = 0
        self.alternate_losses = 0
        self.alternate_draws = 0
        self.alternate_points = 0

    def addRealGame(self, goals_scored, goals_conceded):
        self.actual_goals_scored += goals_scored
        self.actual_goals_conceded += goals_conceded
        if goals_scored > goals_conceded:
            self.actual_wins += 1
            self.actual_points += 3
        elif goals_scored == goals_conceded:
            self.actual_draws += 1
            self.actual_points += 1
        else:
            self.actual_losses += 1

    def addAlternateGame(self, goals_scored, goals_conceded):
        self.alternate_goals_scored += goals_scored
        self.alternate_goals_conceded += goals_conceded
        if goals_scored > goals_conceded:
            self.alternate_wins += 1
            self.alternate_points += 3
        elif goals_scored == goals_conceded:
            self.alternate_draws += 1
            self.alternate_points += 1
        else:
            self.alternate_losses += 1

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return (
            self.name.ljust(33, " ")
            + str(self.alternate_wins)
            + "\t"
            + str(self.alternate_losses)
            + "\t"
            + str(self.alternate_draws)
            + "\t"
            + str(self.alternate_points)
            + "\t"
            + str(self.alternate_goals_scored)
            + "\t"
            + str(self.alternate_goals_conceded)
            + "\t"
            + str(self.alternate_goals_scored - self.alternate_goals_conceded)
            + "\t"
            + str(self.actual_points)
        )


def printTable(Teams):
    print(
        "Team Name".ljust(33, " ")
        + "aW"
        + "\t"
        + "aL"
        + "\t"
        + "aD"
        + "\t"
        + "aP"
        + "\t"
        + "aGF"
        + "\t"
        + "aGA"
        + "\t"
        + "aGD"
        + "\t"
        + "Actual Points"
    )
    print(
        "^^^^ ^^^^".ljust(33, " ")
        + "^^"
        + "\t"
        + "^^"
        + "\t"
        + "^^"
        + "\t"
        + "^^"
        + "\t"
        + "^^^"
        + "\t"
        + "^^^"
        + "\t"
        + "^^^"
        + "\t"
        + "^^^^^^ ^^^^^^"
    )
    print(
        "\n".join(
            [
                str(team)
                for team in sorted(
                    Teams.values(), key=lambda a: a.alternate_points, reverse=True
                )
            ]
        )
    )


def processData(Teams, file_name):
    fin = open(file_name, "r")

    for line in fin:
        (
            home_team,
            away_team,
            home_team_real_score,
            away_team_real_score,
            home_team_alternate_score,
            away_team_alternate_score,
        ) = line.strip().split("\t")

        home_team_real_score = int(home_team_real_score)
        away_team_real_score = int(away_team_real_score)
        home_team_alternate_score = int(home_team_alternate_score)
        away_team_alternate_score = int(away_team_alternate_score)

        Teams[home_team].addRealGame(home_team_real_score, away_team_real_score)
        Teams[away_team].addRealGame(away_team_real_score, home_team_real_score)
        Teams[home_team].addAlternateGame(
            home_team_alternate_score, away_team_alternate_score
        )
        Teams[away_team].addAlternateGame(
            away_team_alternate_score, home_team_alternate_score
        )

    fin.close()


def plotGraph(Teams):
    fig, ax = plt.subplots()
    ax.scatter(
        [Teams[team_name].actual_points for team_name in Teams],
        [Teams[team_name].alternate_points for team_name in Teams],
        s=40,
    )
    ax.plot([0, 114], [0, 114])
    ax.text(10, 80, textwrap.fill("Score early, don't hold onto the leads well", 20))
    ax.text(60, 0, textwrap.fill("Concede early, but do well to fight back", 20))
    ax.axis("scaled")
    ax.set_xlabel("Actual Points")
    ax.set_ylabel("Alternate Points")
    plt.show()


def main():
    team_names = [
        "Liverpool",
        "Norwich City",
        "West Ham",
        "Man. City",
        "Crystal Palace",
        "Everton",
        "Burnley FC",
        "Southampton",
        "Watford",
        "Brighton",
        "Bournemouth",
        "Sheff Utd",
        "Tottenham",
        "Aston Villa",
        "Leicester City",
        "Wolves",
        "Newcastle",
        "Arsenal",
        "Man United",
        "Chelsea",
    ]

    Teams = {team_name: Team(team_name) for i, team_name in enumerate(team_names)}

    processData(Teams, "../Data/PL_201920_Golden_Goal_Data.txt")
    printTable(Teams)
    plotGraph(Teams)


if __name__ == "__main__":
    main()
