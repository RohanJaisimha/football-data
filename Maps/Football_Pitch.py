import numpy as np


def draw_football_pitch(fig, ax, l=120, w=80):
    ax.set_facecolor("xkcd:green")

    # pitch
    ax.plot([0, l, l, 0, 0], [0, 0, w, w, 0], color="white", lw=1)

    # half yard line
    ax.plot([l // 2, l // 2], [w, 0], color="white", lw=1)

    # center circle
    x = np.arange(50, 70, 0.01)
    ax.plot(x, 40 + np.sqrt(100 - (x - 60) ** 2), color="white", lw=1)
    ax.plot(x, 40 - np.sqrt(100 - (x - 60) ** 2), color="white", lw=1)

    # left D
    x = np.arange(18, 22, 0.01)
    ax.plot(x, w // 2 + np.sqrt(100 - (x - 12) ** 2), color="white", lw=1)
    ax.plot(x, w // 2 - np.sqrt(100 - (x - 12) ** 2), color="white", lw=1)

    # left eighteen yard box (18 x 44)
    ax.plot(
        [0, 18, 18, 0],
        [(w - 44) // 2, (w - 44) // 2, (w + 44) // 2, (w + 44) // 2],
        color="white",
        lw=1,
    )

    # left six yard box (6 x 20)
    ax.plot(
        [0, 6, 6, 0],
        [(w - 20) // 2, (w - 20) // 2, (w + 20) // 2, (w + 20) // 2],
        color="white",
        lw=1,
    )

    # right D
    x = np.arange(l - 22, l - 18, 0.01)
    ax.plot(x, w // 2 + (100 - (x + 12 - l) ** 2) ** 0.5, color="white", lw=1)
    ax.plot(x, w // 2 - (100 - (x + 12 - l) ** 2) ** 0.5, color="white", lw=1)

    # right eighteen yard box (18 x 44)
    ax.plot(
        [l, l - 18, l - 18, l],
        [(w - 44) // 2, (w - 44) // 2, (w + 44) // 2, (w + 44) // 2],
        color="white",
        lw=1,
    )

    # right six yard box (6 x 20)
    ax.plot(
        [l, l - 6, l - 6, l],
        [(w - 20) // 2, (w - 20) // 2, (w + 20) // 2, (w + 20) // 2],
        color="white",
        lw=1,
    )

    # left penalty spot
    ax.plot(12, w // 2, "wo", ms=2)

    # right penalty spot
    ax.plot(l - 12, w // 2, "wo", ms=2)

    # center spot
    ax.plot(l // 2, w // 2, "wo", ms=2)

    # left goal
    ax.plot(
        [0, -4, -4, 0],
        [(w - 8) // 2, (w - 8) // 2, (w + 8) // 2, (w + 8) // 2],
        color="white",
        lw=1,
    )

    # right goal
    ax.plot(
        [l, l + 4, l + 4, l],
        [(w - 8) // 2, (w - 8) // 2, (w + 8) // 2, (w + 8) // 2],
        color="white",
        lw=1,
    )

    # bottom left corner arc
    x = np.arange(0.5, 1.5, 0.01)
    ax.plot(x, 0.5 + np.sqrt(1 - (x - 0.5) ** 2), color="white", lw=1)

    # top left corner arc
    x = np.arange(0, 1.01, 0.01)
    ax.plot(x + 0.5, w - np.sqrt(1 - x ** 2), color="white", lw=1)

    # top right corner arc
    x = np.arange(l - 1, l + 0.01, 0.01)
    ax.plot(x, w - np.sqrt(1 - (x - l) ** 2), color="white", lw=1)

    # bottom right corner arc
    x = np.arange(l - 1, l + 0.01, 0.01)
    ax.plot(x, np.sqrt(1 - (x - l) ** 2), color="white", lw=1)
